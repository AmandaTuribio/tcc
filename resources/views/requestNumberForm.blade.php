@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Numero do Edital</p></div>
			<div class="panel-body putmargin">
				<form class="form-horizontal my-from" method="POST" action="{{ route('notice.storeNumber', $notice->id) }}">
					{{ csrf_field() }}
					<label for="number" class="my-form-label">Informe o numero do edital:</label>
					<input type="text" class="form-control" name="number">
					<br>
					<br>
					<div class="form-group col-md-12">
						<button type="submit" class="btn bnt-padrao col-md-3">
							ENVIAR
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
