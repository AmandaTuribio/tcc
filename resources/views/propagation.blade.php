@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Divulgação</p></div>
			<div class="panel-body putmargin">
				@if (isset($propagation->id))
				<form class="form-horizontal" method="POST" action="{{ route('propagation.update', $propagation->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
				@else
					<form class="form-horizontal my-from" method="POST" action="{{ route('propagation.store') }}">
						{{ csrf_field() }}
						@endif
						<input type="hidden" name="id" value="{{ $propagation->id ?  $propagation->id : '' }}" />
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="my-form-label">Nome</label>
							<input id="name" type="text" class="form-control" name="name" value="{{ $propagation->name ?  $propagation->name : old('name') }}" required autofocus>
							@if ($errors->has('name'))
							<span>
								<strong class="help-block">{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="my-form-label">E-Mail</label>
							<div class="input-field">
								<input id="email" type="email" class="form-control" name="email" value="{{ $propagation->email ?  $propagation->email : old('email') }}" required>
							</div>
							@if ($errors->has('email'))
							<span>
								<strong class="help-block">{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group">
							<button type="submit" class="btn bnt-padrao col-md-3">
								ENVIAR
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection
