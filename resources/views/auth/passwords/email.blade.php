@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Resetar Senha</p></div>
			<div class="panel-body putmargin">
				<form class="form-horizontal my-from" method="POST" action="{{ route('password.email') }}">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="my-form-label">E-Mail</label>

						<div class="input-field">
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn bnt-padrao col-md-3">
							ENVIAR LINK
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
