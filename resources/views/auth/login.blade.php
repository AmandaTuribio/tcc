@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Login</p></div>
			<div class="panel-body putmargin">
				<form class="form-horizontal my-from" method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="my-form-label">E-Mail</label>

						<div class="input-field">
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="my-form-label">Senha</label>

						<div class="input-field">
							<input id="password" type="password" class="form-control" name="password" required>

							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn bnt-padrao col-md-3">
							LOGIN
						</button>
						<a class="btn" href="{{ route('password.request') }}">
							Esqueceu sua Senha?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
