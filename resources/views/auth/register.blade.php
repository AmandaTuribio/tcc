@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Cadastro de Funcionario</p></div>
			<div class="panel-body putmargin">
				<form class="form-horizontal my-from" method="POST" action="{{ route('register') }}">
					{{ csrf_field() }}
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="my-form-label">Nome</label>

						<div class="input-field">
							<input class="form-control" id="name" type="text"  name="name" value="{{ old('name') }}" required>

							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="my-form-label">E-Mail</label>

						<div class="input-field">
							<input class="form-control" id="email" type="email"  name="email" value="{{ old('email') }}" required>

							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="my-form-label">Senha</label>

						<div class="input-field">
							<input class="form-control" id="password" type="password" name="password" required>

							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<label for="password-confirm" class="my-form-label">Confirmar Senha</label>

						<div class="input-field">
							<input class="form-control" id="password-confirm" type="password" name="password_confirmation" required>
						</div>
					</div>
					<div class="form-group{{ $errors->has('coordinator') ? ' has-error' : '' }}">
						<input class="custom-control-input" type="checkbox" id="coordinator" name="coordinator"  @if ( old('coordinator')) checked @endif value="1"  />
						<label for="coordinator" class="my-form-label">Coordenador</label>
						@if ($errors->has('coordinator'))
						<span class="help-block">
							<strong>{{ $errors->first('coordinator') }}</strong>
						</span>
						@endif
					</div>
					<br>
					<div class="form-group">
						<button type="submit" class="btn bnt-padrao col-md-3">
							CADASTRAR
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
