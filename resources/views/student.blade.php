@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Dados do Aluno</p></div>
			<div class="panel-body putmargin">
                    @if (isset($user->id))
                    <form class="form-horizontal my-from" method="POST" action="{{ route('student.update', $user->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        @else
                        <form class="form-horizontal my-from" method="POST" action="{{ route('student.store') }}">
                            {{ csrf_field() }}
                            @endif
                            <input type="hidden" name="id" value="{{ $user->id ?  $user->id : '' }}" />
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="my-form-label">Nome</label>

                                <div class="input-field">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name ?  $user->name : old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                    <span>
                                        <strong class="red-text">{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="my-form-label">E-Mail</label>

                                <div class="input-field">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email ?  $user->email : old('email') }}" required>

                                    @if ($errors->has('email'))
                                    <span>
                                        <strong class="red-text">{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @if (!isset($user->id))
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="my-form-label">Senha</label>
                                <div class="input-field">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                    <span>
                                        <strong class="red-text">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="my-form-label">Confirmar Senha</label>

                                <div class="input-field">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            @endif
                            @if (isset($user->id))
                            <div class="form-group">
                                <input type="checkbox" class="custom-control-input" id="active" name="active"   @if ($user->active == true) checked @endif value="1" />
                                       <label for="active" class="my-form-label">Ativo</label>
                                @if ($errors->has('active'))
                                <span>
                                    <strong class="red-text">{{ $errors->first('active') }}</strong>
                                </span>
                                @endif
                            </div>
                            @endif
							<div class="form-group{{ $errors->has('rg') ? ' has-error' : '' }}">
								<label for="rg" class="my-form-label">RG</label>
								<div class="input-field">
									<input id="rg" type="text" class="form-control" name="rg" value="{{ $student->rg ?  $student->rg : old('rg') }}" required>

									@if ($errors->has('rg'))
									<span>
										<strong class="red-text">{{ $errors->first('rg') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('rg') ? ' has-error' : '' }}">
									<label for="cpf" class="my-form-label">CPF</label>
								<div class="input-field">
									<input id="cpf" type="text" class="form-control" name="cpf" value="{{ $student->cpf ?  $student->cpf : old('cpf') }}" required>

									@if ($errors->has('cpf'))
									<span>
										<strong class="red-text">{{ $errors->first('cpf') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
									<label for="gender" class="my-form-label">Gênero</label><br>
                                <input type="radio" class="custom-control-input" id="gender" name="gender"   @if ($student->gender == true) checked @endif value="1" />
                            	<label for="active" class="my-form-label">Masculino</label>
								<input type="radio" class="custom-control-input" id="gender" name="gender"   @if ($student->gender == true) checked @endif value="2" />
                            	<label for="active" class="my-form-label">Feminino</label>
                                @if ($errors->has('gender'))
                                <span>
                                    <strong class="red-text">{{ $errors->first('active') }}</strong>
                                </span>
                                @endif
                            </div>
							<div class="form-group{{ $errors->has('mother') ? ' has-error' : '' }}">
									<label for="mother" class="my-form-label">Nome da Mãe</label>
								<div class="input-field">
									<input id="mother" type="text" class="form-control" name="mother" value="{{ $student->mother ?  $student->mother : old('mother') }}" required>

									@if ($errors->has('mother'))
									<span>
										<strong class="red-text">{{ $errors->first('mother') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('father') ? ' has-error' : '' }}">
									<label for="father" class="my-form-label">Nome do Pai</label>
								<div class="input-field">
									<input id="father" type="text" class="form-control" name="father" value="{{ $student->father ?  $student->father : old('father') }}" required>

									@if ($errors->has('father'))
									<span>
										<strong class="red-text">{{ $errors->first('father') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                <label for="street" class="my-form-label">Logradouro</label>
                                <div class="input-field">
                                    <input id="street" type="text" class="form-control" name="street" value="{{ $student->street ?  $student->street : old('street') }}" required>

                                    @if ($errors->has('street'))
                                    <span>
                                        <strong class="red-text">{{ $errors->first('street') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
							<div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
								<label for="number" class="my-form-label">Número</label>
								<div class="input-field">
									<input id="rg" type="number" class="form-control" name="number" value="{{ $student->number ?  $student->number : old('number') }}" required>

									@if ($errors->has('number'))
									<span>
										<strong class="red-text">{{ $errors->first('number') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('Apto') ? ' has-error' : '' }}">
									<label for="Apto" class="my-form-label">Complemento</label>
								<div class="input-field">
									<input id="Apto" type="text" class="form-control" name="Apto" value="{{ $student->Apto ?  $student->Apto : old('Apto') }}" required>

									@if ($errors->has('Apto'))
									<span>
										<strong class="red-text">{{ $errors->first('Apto') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('neighborhood') ? ' has-error' : '' }}">
								<label for="neighborhood" class="my-form-label">Bairro</label>
								<div class="input-field">
									<input id="neighborhood" type="text" class="form-control" name="neighborhood" value="{{ $student->neighborhood ?  $student->neighborhood : old('neighborhood') }}" required>

									@if ($errors->has('neighborhood'))
									<span>
										<strong class="red-text">{{ $errors->first('neighborhood') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
								<label for="city" class="my-form-label">Cidade</label>
								<div class="input-field">
									<input id="city" type="text" class="form-control" name="city" value="{{ $student->city ?  $student->city : old('city') }}" required>

									@if ($errors->has('city'))
									<span>
										<strong class="red-text">{{ $errors->first('city') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
								<label for="cep" class="my-form-label">CEP</label>
								<div class="input-field">
									<input id="cep" type="text" class="form-control" name="cep" value="{{ $student->cep ?  $student->cep : old('cep') }}" required>

									@if ($errors->has('cep'))
									<span>
										<strong class="red-text">{{ $errors->first('cep') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('ddd') ? ' has-error' : '' }}">
									<label for="ddd" class="my-form-label">DDD</label>
								<div class="input-field">
									<input id="ddd" type="number" class="form-control" name="ddd" value="{{ $student->ddd ?  $student->ddd : old('ddd') }}" required>

									@if ($errors->has('ddd'))
									<span>
										<strong class="red-text">{{ $errors->first('ddd') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
									<label for="phone" class="my-form-label">Telefone</label>
								<div class="input-field">
									<input id="phone" type="text" class="form-control" name="phone" value="{{ $student->phone ?  $student->phone : old('phone') }}" required>

									@if ($errors->has('phone'))
									<span>
										<strong class="red-text">{{ $errors->first('phone') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('celphone') ? ' has-error' : '' }}">
									<label for="celphone" class="my-form-label">Celular</label>
								<div class="input-field">
									<input id="celphone" type="text" class="form-control" name="celphone" value="{{ $student->celphone ?  $student->celphone : old('celphone') }}" required>

									@if ($errors->has('celphone'))
									<span>
										<strong class="red-text">{{ $errors->first('celphone') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('birthDate') ? ' has-error' : '' }}">
									<label for="birthDate" class="my-form-label">Data de nascimento</label>
								<div class="input-field">
									<input id="birthDate" type="text" class="form-control" name="birthDate" value="{{ $student->birthDate ?  $student->birthDate : old('birthDate') }}" required>

									@if ($errors->has('birthDate'))
									<span>
										<strong class="red-text">{{ $errors->first('birthDate') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('birthCity') ? ' has-error' : '' }}">
									<label for="birthCity" class="my-form-label">Naturalidade</label>
								<div class="input-field">
									<input id="birthCity" type="text" class="form-control" name="birthCity" value="{{ $student->birthCity ?  $student->birthCity : old('birthCity') }}" required>

									@if ($errors->has('birthCity'))
									<span>
										<strong class="red-text">{{ $errors->first('birthCity') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('uf') ? ' has-error' : '' }}">
                                <label for="uf" class="my-form-label">Estado</label>

                                <div>
                                    <select name="uf" class="form-control">
                                        <option value="" disabled selected>Selecione</option>

										@foreach( $ufs as $uf)
										<option value="{{$uf}}"
											@if($student->uf == $uf)
											selected
											@endif
										>{{$uf}}</option>
										@endforeach

                                    </select>
                                </div>
                            </div>

                            <br />
                              <div class="form-group">
                                <button type="submit" class="btn bnt-padrao col-md-3">
                                  ENVIAR
                                </button>
                              </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
