@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@can('has-permission', 'employee')
			<div class="tituloPag"><p>Turmas Abertas</p></div>
			<div class="panel-body putmargin">
				<vc-paginate  source="/tcc/public/api/turma/index/abertas"  Componente="RequestedTurma" auth="avaliada"></vc-paginate>
			</div>
			@endcan
		</div>
	</div>
</div>
@endsection
