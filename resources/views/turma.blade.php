@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Turmas</p></div>
			<div class="panel-body putmargin">
				@if (isset($turma->id))
				<form class="form-horizontal  my-from" method="POST" action="{{ route('turma.update', $turma->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					@else
					<form class="form-horizontal  my-from" method="POST" action="{{ route('turma.store') }}">
						{{ csrf_field() }}
						@endif
						<input type="hidden" value="{{$course->id}}" name="course_id">

						<div class="form-group{{ $errors->has('cname') ? ' has-error' : '' }}">
							<label for="cname" class="my-form-label">Curso</label>

							<div class="input-field">
								<input id="cname" type="text" class="form-control" name="cname" value="{{ $course->name ?  $course->name : old('cname') }}" disabled required autofocus>

								@if ($errors->has('cname'))
								<span>
									<strong class="help-block">{{ $errors->first('cname') }}</strong>
								</span>
								@endif
							</div>
						</div>


						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="my-form-label">Nome da Turma</label>

							<div class="input-field">
								<input id="name" type="text" class="form-control" name="name" value="{{ $turma->name ?  $turma->name : old('name') }}" required autofocus>

								@if ($errors->has('name'))
								<span>
									<strong class="help-block">{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('startTime') ? ' has-error' : '' }}">
							<label for="startTime" class="my-form-label">Horario de inicio das aulas</label>

							<div class="input-field">
								<input id="startTime" type="text" class="form-control" name="startTime" value="{{ $turma->startTime ?  $turma->startTime : old('startTime') }}" required autofocus>

								@if ($errors->has('startTime'))
								<span>
									<strong class="help-block">{{ $errors->first('startTime') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('closingTime') ? ' has-error' : '' }}">
							<label for="closingTime" class="my-form-label">Horario de termino das aulas</label>

							<div class="input-field">
								<input id="closingTime" type="text" class="form-control" name="closingTime" value="{{ $turma->closingTime ?  $turma->closingTime : old('closingTime') }}" required autofocus>

								@if ($errors->has('closingTime'))
								<span>
									<strong class="help-block">{{ $errors->first('closingTime') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('d') ? ' has-error' : '' }}">
							<label class="my-form-label">Selecione os dias</label>
							<select class="form-control" name="d[]" multiple>
								@foreach ($days as $day  => $value)
								<option value="{{$day}}"
								@foreach ($turma->days as $d)
								{{ $d->description == $value ?  'selected' : '' }}
								@endforeach
								>{{$value}}</option>
								@endforeach
							</select>
							@if ($errors->has('d'))
							<span class="help-block">
								<strong>{{ $errors->first('d') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('selectiveProcess') ? ' has-error' : '' }}">
							<label class="my-form-label">Processo Seletivo</label>
							<div class="input-field">
								<select class="form-control" name="selectiveProcess" autofocus>
									<option value="" disabled selected>Selecione o Tipo de processo Seletivo</option>
									<option value="sorteio" {{ $turma->selectiveProcess == 'sorteio' ?  'selected' : '' }}> Sorteio</option>
									<option value="prova" {{ $turma->selectiveProcess == 'prova' ?  'selected' : '' }}> Prova</option>
									<option value="ordemInscricao"  {{ $turma->selectiveProcess == 'ordemInscricao' ?  'selected' : '' }}>Ordem de Incrição</option>
								</select>
							</div>
							@if ($errors->has('selectiveProcess'))
							<span class="help-block">
								<strong>{{ $errors->first('selectiveProcess') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('classroom') ? ' has-error' : '' }}">
							<label for="classroom" class="my-form-label">Sala de aula</label>
							<div class="input-field">
								<input id="classroom" type="text" class="form-control" name="classroom" value="{{ $turma->classroom ?  $turma->classroom : old('classroom') }}" required>

								@if ($errors->has('classroom'))
								<span>
									<strong class="help-block">{{ $errors->first('classroom') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
							<label for="startdate" class="my-form-label">Data de inicio das aulas</label>
							<div class="input-field">
								<input id="startdate" type="text" class="form-control" name="startdate" value="{{ $turma->startdate ?  $turma->startdate : old('startdate') }}" required>

								@if ($errors->has('startdate'))
								<span>
									<strong class="help-block">{{ $errors->first('startdate') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('closingdate') ? ' has-error' : '' }}">
							<label for="closingdate" class="my-form-label">Data de termino das aulas</label>
							<div class="input-field">
								<input id="closingdate" type="text" class="form-control" name="closingdate" value="{{ $turma->closingdate ?  $turma->closingdate : old('closingdate') }}" required>

								@if ($errors->has('closingdate'))
								<span>
									<strong class="help-block">{{ $errors->first('closingdate') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('numberVacancies') ? ' has-error' : '' }}">
							<label for="numberVacancies" class="my-form-label">Numero de Vagas</label>
							<div class="input-field">
								<input id="numberVacancies" type="number" class="form-control" name="numberVacancies" value="{{ $turma->numberVacancies ?  $turma->numberVacancies : old('numberVacancies') }}" required>

								@if ($errors->has('numberVacancies'))
								<span>
									<strong class="help-block">{{ $errors->first('numberVacancies') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group col-md-12">
							<button type="submit" class="btn bnt-padrao col-md-3">
								ENVIAR
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection
