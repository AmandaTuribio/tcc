@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Dados do Proponente</p></div>
			<div class="panel-body putmargin">
				@if (isset($user->id))
				<form class="form-horizontal my-from" method="POST" action="{{ route('proponent.update', $user->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					@else
					<form class="form-horizontal my-from" method="POST" action="{{ route('proponent.store') }}">
						{{ csrf_field() }}
						@endif
						<input type="hidden" name="id" value="{{ $user->id ?  $user->id : '' }}" />
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<div class="input-field">
								<input placeholder="Nome:" id="name" type="text" class="form-control" name="name" value="{{ $user->name ?  $user->name : old('name') }}" required autofocus>

								@if ($errors->has('name'))
								<span>
									<strong class="help-block">{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<div class="input-field">
								<input placeholder="E-Mail:" id="email" type="email" class="form-control" name="email" value="{{ $user->email ?  $user->email : old('email') }}" required>

								@if ($errors->has('email'))
								<span>
									<strong class="help-block">{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
						</div>

						@if (!isset($user->id))
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<div class="input-field">
								<input placeholder="Senha:" id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
								<span>
									<strong class="help-block">{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="input-field">
								<input placeholder="Confirmar senha:"id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>
						@endif


						<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
							<div>
								<select name="typeProp" class="form-control">
									<option value="" disabled selected>Selecione o proponente</option>
									<option value="Aluno" @if(isset($user->proponent) && $user->proponent->type == 'Aluno' || old('typeProp') == 'Aluno'  ) selected @endif >Aluno</option>
									<option value="Servidor" @if(isset($user->proponent) && $user->proponent->type == 'Servidor' || old('typeProp') == 'Servidor') selected @endif>Servidor</option>
									<option value="Professor" @if(isset($user->proponent) && $user->proponent->type == 'Professor' || old('typeProp') == 'Professor') selected @endif>Professor</option>
								</select>
							</div>
						</div>
						@if (isset($user->id))
						<div class="form-group">
							<input type="checkbox" class="custom-control-input" id="active" name="active"   @if ($user->active == true) checked @endif value="1" />
							<label for="active" class="my-form-label">Ativo</label>
							@if ($errors->has('active'))
							<span>
								<strong class="help-block">{{ $errors->first('active') }}</strong>
							</span>
							@endif
						</div>
						@endif
						<br />
						<div class="form-group">
							<button type="submit" class="btn bnt-padrao col-md-3">
								ENVIAR
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
