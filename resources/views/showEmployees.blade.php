@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@can('has-permission', 'employee')
			<div class="tituloPag"><p>Funcionarios</p></div>
			<div class="panel-body putmargin">
				<vc-paginate source="/tcc/public/api/employee" Componente="Employee"></vc-paginate>
			</div>
			@endcan
		</div>
	</div>
</div>
@endsection
