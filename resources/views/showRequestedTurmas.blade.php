@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Solicitações de turmas</p></div>
			<div class="panel-body putmargin">
				@if(Auth::user()->type == 'employee')
					<vc-paginate  source="/tcc/public/api/requested/turmas"  Componente="RequestedTurma"></vc-paginate>
				@elseif(Auth::user()->type == 'proponent')
					<vc-paginate source="/tcc/public/api/requested/turmas/toAprove" Componente="RequestedTurma"></vc-paginate>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
