<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="{{ asset('css/style.css') }}"type="text/css" rel="stylesheet" media="screen,projection"/>
	<style media="screen">
	ol {
		counter-reset: item
	}
	li{
		display: block
	}
	li:before {
		content: counters(item, ".") " ";
		counter-increment: item
	}
	body{
		margin-top: 1%;
		margin-right: 5%;
	}
	p{
		margin-left: 20px;
		margin-right: 20px;
		text-align: justify;
		color: black;
	}
	.titulo{
		font-size: 11px;
		margin-top: 10%;
		font-size: 200%;
		text-align: center;
		color: rgba(0, 0, 0, 0.7);
	}
	.imagem{
		width: 100px;
		height: auto;
	}
	table {
		margin-top: 1%;
		width:100%;
		border-collapse: collapse;
		margin-bottom: 2%;
		text-align: center;
	}
	table tr td{
		text-align: center;
		border:1px solid black;
	}
	.negrito{
		font-weight: bold;
	}
	</style>
</head>
<body>
	<center>
		<img class="imagem" src="{{asset('images/image1.jpg')}}">
	</center>
	<h6 class="center"><b>MINISTÉRIO DA EDUCAÇÃO SECRETARIA DE EDUCAÇÃO PROFISSIONAL E TECNOLÓGICA  INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DE SÃO PAULO CAMPUS GUARULHOS</b></h6>
	<br>


	<h6 class="center"><b>EDITAL Nº {{$notice->number}}, DE
		{{strtoupper(strftime('%d DE %B DE %Y', strtotime($notice->data)))}}
	</b></h6>
	<br>

	<h6 class="center"><b>PROCESSO SELETIVO SIMPLIFICADO PARA O PREENCHIMENTO DE VAGAS DO <br>CURSO DE EXTENSÃO
	</b></h6>

	<p>A Diretoria-Geral do Câmpus Guarulhos do Instituto Federal de Educação, Ciência e Tecnologia de São Paulo (IFSP), por meio de sua Coordenadoria de Extensão, no uso de suas atribuições legais, torna público que estão abertas as inscrições para o curso de extensão, conforme instruções do presente Edital. </p>
	<ol>
		<li> <b>DO CURSO, NÚMERO DE VAGAS, PRÉ-REQUISITOS E DURAÇÃO</b>
			<ol>
				<li>O curso oferecido, bem como o número de vagas e os pré-requisitos estão descritos no quadro abaixo.</li>
				<table>
					<tr class="negrito">
						<td>Curso</td>
						<td>Pré-requisitos</td>
						<td>Vagas Ampla Concorrência</td>
						<td colspan="2">Reserva de Vagas
							(25%  Candidatos Negros/Pardos)
							e
							(5% Candidatos com Deficiência)
						</td>
					</tr>
					@foreach ($notice->turmas as $turma)
					<tr>
						<td>{{$turma->course->name}} - {{$turma->name}}</td>
						<td>{{$turma->course->preRequirements}}</td>
						<td>@php
							$vagas = $turma->numberVacancies - 25* $turma->numberVacancies /100 - 5* $turma->numberVacancies /100;
							echo $vagas;
							@endphp
						</td>
						<td>@php
							$vagas = 25* $turma->numberVacancies /100;
							echo "$vagas vagas para candidatos autodeclarados negros ou pardos";
							@endphp
						</td>
						<td>@php
							$vagas = 5* $turma->numberVacancies /100;
							echo "$vagas vaga para candidatos com deficiência";
							@endphp
						</td>
					</tr>
					@endforeach
				</table>
				<li>O curso oferecido terá duração e carga horária conforme definido no quadro abaixo:</li>
				<table>
					<tr class="negrito">
						<td>Curso</td>
						<td>Duração</td>
						<td>Horário</td>
						<td>Carga horária total</td>
					</tr>
					@foreach ($notice->turmas as $turma)
					<tr>
						<td>{{$turma->course->name}}</td>
						<td>Início em <b>{{strftime('%d/%m/%Y', strtotime($turma->startdate))}}</b> Término previsto para <b>{{strftime('%d/%m/%Y', strtotime($turma->closingdate))}}</b></td>
						<td>Às
							@foreach ($turma->days as $day)
							{{$day->description}}
							@endforeach
							das {{$turma->startTime}} às {{$turma->closingTime}}
						</td>
						<td>{{$turma->course->workload}} horas</td>
					</tr>
					@endforeach
				</table>
				<li>Cinco por cento (5%) das vagas são reservadas a candidatos com deficiência.</li>
				<li>Vinte e cinco por cento (25%) das vagas são reservadas a candidatos negros</li>
				<li>Poderão concorrer às vagas reservadas a candidatos negros aqueles que se autodeclararem pretos ou pardos no ato da inscrição, conforme o quesito cor ou raça utilizado pela Fundação Instituto Brasileiro de Geografia e Estatística – IBGE.</li>
				<li>Caso não haja o preenchimento de pelo menos setenta por cento (70%) das vagas previstas para o curso, o Câmpus Guarulhos do IFSP se reserva o direito de decidir sobre a sua oferta.</li>
			</ol>
		</li>
		<li>DA INSCRIÇÃO
			<ol>
				<li>Poderão se inscrever no processo seletivo os candidatos que se adequarem aos pré-requisitos estabelecidos no Item 1.1 deste Edital.</li>
				<li>Não haverá cobrança de taxa de inscrição.</li>
				<li>As inscrições serão realizadas pelo Sitema CEX, período de <b>{{strftime('%d/%m/%Y', strtotime($notice->startdate))}}</b> a  <b>{{strftime('%d/%m/%Y', strtotime($notice->closingdate))}}</b>. </li>
				<li>Os candidatos com deficiência deverão, obrigatoriamente, comprovar sua condição mediante a submissão de laudo médico recente, emitido por profissional da área, na inscrição.</li>
				<li>Serão anuladas, a qualquer tempo, as inscrições que não obedeçam às determinações contidas neste Edital.</li>
			</ol>
		</li>
		<li>DA SELEÇÃO
			<ol>
				@foreach ($notice->turmas as $turma)
				@switch($turma->selectiveProcess)
				@case('ordemInscricao')
				<li>A seleção dos candidatos ocorrerá por Ordem de Inscrição que classificará, automaticamente, os primeiros inscritos que atenderem aos requisitos estabelecidos no Item 1.1 deste Edital.</li>
				@break

				@case('prova')
				<li>A seleção dos candidatos ocorrerá por meio da realização de uma prova, a qual ocorrerá no dia {{$notice->testDay}} que classificará, automaticamente, os primeiros classificados que atenderem aos requisitos estabelecidos no Item 1.1 deste Edital.</li>
				@break

				@case('sorteio')
				<li>A seleção dos candidatos ocorrerá por meio meio da realização de um sorteio que classificará, automaticamente, os primeiros sorteados que atenderem aos requisitos estabelecidos no Item 1.1 deste Edital.</li>
				@break
				@endswitch
				@endforeach
				<li>A seleção dos candidatos obedecerá a divisão de vagas no que se refere aos quesitos Ampla concorrência e Reserva de vagas, conforme definido no Item 1.1 deste Edital.</li>
				<li>No caso da falta de candidatos para ocupar uma vaga reservada, serão convocados os candidatos classificados dentro do número de vagas para ampla concorrência.</li>
				<li>O processo seletivo classificará a quantidade de candidatos correspondente ao número de vagas estabelecidas mais o equivalente a 20% do número de vagas para composição de lista de espera.</li>
				<li>Os candidatos cadastrados na lista de espera poderão ser chamados em caso de desistências decorridas em até 25% da carga horária do curso.</li>
				<li>Serão aplicados os mesmos critérios de reserva de vagas na lista de espera.</li>
			</ol>
		</li>
		<li>DO RESULTADO
			<ol>
				<li>O resultado a relação dos candidatos contemplados, bem como da lista de espera, será amplamente divulgada pelo câmpus Guarulhos do IFSP mediante fixação de lista nos murais e nos endereços eletrônicos http://portal.ifspguarulhos.edu.br e SISTEMA CEX no dia <b>{{strftime('%d/%m/%Y', strtotime($notice->postResult))}}</b>.</li>
				<li>A divulgação do resultado indicará os candidatos classificados dentro do número de vagas para a ampla concorrência e os candidatos que concorreram às vagas reservadas.</li>
			</ol>
		</li>
		<li>DA MATRÍCULA
			<ol>
				@foreach ($notice->turmas as $turma)
				@switch($turma->selectiveProcess)
				@case('ordemInscricao')
				<li>As matrículas serão realizadas no ato da inscrição, por ordem de inscrição, desde que o candidato comprove todos os requisitos do item 1.1, atravez do Sistema CEX no período de <b>{{strftime('%d/%m/%Y', strtotime($notice->registrationstartdate))}}</b> a <b>{{strftime('%d/%m/%Y', strtotime($notice->registrationclosingdate))}}</b>, ou até o preenchimento das vagas.</li>
				@break

				@case('prova')
				@case('sorteio')
				<li>As matrículas serão realizadas atravez do Sistema CEX no período de <b>{{strftime('%d/%m/%Y', strtotime($notice->registrationstartdate))}}</b> a <b>{{strftime('%d/%m/%Y', strtotime($notice->registrationclosingdate))}}</b>, ou até o preenchimento das vagas.</li>
				@break
				@endswitch
				@endforeach
				<li>Não haverá cobrança de taxa de matrícula.</li>
				<li>No ato da matricula deverá se realizado o upload da documentação necessária, sendo que a não apresentação de quaisquer dos documentos exigidos levará à perda da vaga e ao não aceite da matrícula.</li>
				<li>Os documentos exigidos são:
					<ol>
						<li>Carteira de identidade ou documento oficial com foto;</li>
						<li>CPF;</li>
						<li>Comprovante de endereço recente;</li>
						<li>Autorização dos pais ou responsável legal no caso de o candidato ser menor de idade;</li>
						<li>Laudo médico no caso de candidatos que queiram concorrer à(s) vaga(s) específica(s);</li>
						<li>Todoos os demais documento necessaios para comprovar os pre-requisitos do curso.</li>
					</ol>
				</li>
			</ol>
		</li>
		<li>DO INÍCIO DO CURSO
			<ol>
				<li>O início do curso está previsto no item 1.2. Esta data poderá sofrer alteração ou ser prorrogada, se necessário. </li>
			</ol>
		</li>
		<li>DA CERTIFICAÇÃO
			<ol>
				<li>Somente terá direito ao certificado o aluno que obtiver o mínimo de 75% de frequência global no curso e aproveitamento suficiente, conforme critérios de avaliação definidos no Projeto Pedagógico do Curso (PPC).</li>
			</ol>
		</li>
		<li>DAS DISPOSIÇÕES GERAIS
			<ol>
				<li> O candidato inscrito assume a aceitação total das normas constantes neste Edital.</li>
				<li>Caberá à Coordenadoria de Extensão do Câmpus Guarulhos do IFSP a responsabilidade de zelar pela lisura do processo seletivo.</li>
				<li>Os casos omissos, não previstos neste Edital, serão julgados pelo Coordenador de Extensão do Câmpus Guarulhos do IFSP.</li>
			</ol>
		</li>
		<li>DO CRONOGRAMA
			<table class="left">
				<tr class="negrito">
					<td>EVENTO</td>
					<td>DATA / PERÍODO</td>
				</tr>
				<tr>
					<td>Período para inscrições</td>
					<td>{{strftime('%d/%m/%Y', strtotime($notice->startdate))}} a {{strftime('%d/%m/%Y', strtotime($notice->closingdate))}}</td>
				</tr>
				<tr>
					<td>Divulgação do resultado</td>
					<td>{{strftime('%d/%m/%Y', strtotime($notice->postResult))}}</td>
				</tr>
				<tr>
					<td>Data prevista para o início do curso </td>
					<td>
						@foreach ($notice->turmas as $turma)
					{{$turma->course->name}}: {{strftime('%d/%m/%Y', strtotime($turma->startdate))}} <br />
						@endforeach
					</td>
				</tr>

			</table>
		</li>
	</ol>
	<center>
		Ricardo Agostinho de Rezende Junior
			Diretor Geral- IFSP Câmpus Guarulhos
	</center>
	</body>
	</html>
