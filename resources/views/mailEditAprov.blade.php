<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>IFSP - Extensão</title>
</head>
<body>
	<div class="container">
		<center>
			<div style="height:20px;">
				<h1>Sistema CEX</h1>
			</div>
		</center>
		<h2> Olá!!</h2><br/>
		@if($turma->status == 'cancelada')
		<p>
			Sua solicitação para abertura de da turma {{$turma->name}} realizada no dia {{strftime('%d de %B de %Y', strtotime($turma->date))}}
		</p>
		@else
		<p> Sua solição para a abertura da {{$turma->name}} realizada no dia {{strftime('%d DE %B DE %Y', strtotime($turma->date))}} foi alterada. É necessaria a aprovação das alterações para dar andamento no processo. <a href="{{$link}}">Veja as alterações</a><br>
		</p>
		@endif
		<br>
		<p>	Com os melhores cumprimentos <br>
			CEX- Guarulhos
		</p>
		<center>
			<div style="height:20px;">
				<p>@CEX Todos os direitos reservados</p>
			</div>
		</center>

	</div>

</body>
</html>
