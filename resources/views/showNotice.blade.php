@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@can('has-permission', 'employee')
			<div class="tituloPag"><p>Editais em andamento</p></div>
			<div class="panel-body putmargin">
				<vc-paginate  source="/tcc/public/api/notice" type="andamento" title="Editais" Componente="Notice"></vc-paginate>
			</div>
			@endcan
		</div>
	</div>
</div>
@endsection
