@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Meus Dados</p></div>
			<div class="panel-body putmargin">
				<form class="form-horizontal my-from" method="POST" action="{{ route('user.update', $user->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<input type="hidden" name="id" value="{{ $user->id }}" />
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="my-form-label">Nome</label>
						<div class="input-field">
							<input id="name" type="text" class="form-control" name="name" value="{{ $user->name ?  $user->name : old('name') }}" required autofocus>

							@if ($errors->has('name'))
							<span>
								<strong class="help-block">{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="my-form-label">E-Mail</label>
						<div class="input-field">
							<input id="email" type="email" class="form-control" name="email" value="{{ $user->email ?  $user->email : old('email') }}" required>

							@if ($errors->has('email'))
							<span>
								<strong class="help-block">{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="my-form-label">Nova Senha</label>
						<div class="input-field">
							<input id="password" type="password" class="form-control" name="password">

							@if ($errors->has('password'))
							<span>
								<strong class="help-block">{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						<label for="password_confirmation" class="my-form-label">Confirme Senha</label>
						<div class="input-field">
							<input id="password_confirmation" type="password" class="form-control" name="password_confirmation">

							@if ($errors->has('password_confirmation'))
							<span>
								<strong class="help-block">{{ $errors->first('password_confirmation') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn bnt-padrao col-md-3">
							ENVIAR
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
