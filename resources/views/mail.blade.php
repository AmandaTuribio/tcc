<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>IFSP - Extensão</title>
</head>
<body>
	<div class="container">
		<center>
			<div style="height:20px;">
				<h1>Sistema CEX</h1>
			</div>
		</center>
		<h2> Olá!!</h2><br/>
		@if($proponent->status == 'Solicitado')
		<p>
			Sua solicitação para obter acesso foi enviada para avovação. Aguarde resposta.
		</p>
		@else
		<p> Sua Solicitação foi Apovada!
			Você pode acessar o Sistema CEX utilizando o email {{$proponent->user->email}} e a senha informada no momento do cadastro.<br />
			Caso não tenha acesso ao dados do cadastro solicite alteração de senha  disponivel na tela de login.
		</p>
		@endif
		<br>
		<p>	Com os melhores cumprimentos <br>
			CEX- Guarulhos
		</p>
		<center>
			<div style="height:20px;">
				<p>@CEX Todos os direitos reservados</p>
			</div>
		</center>

	</div>

</body>
</html>
