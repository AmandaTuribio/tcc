<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.titulo', 'Sistema CEX') }}</title>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link href="{{ asset('css/layouts.css') }}" rel="stylesheet">
</head>
<body>
	<nav class="navbar-default marg">
		<div class="container">
			<div class="navbar-header">
				<!-- Collapsed Hamburger -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div>
				<ul class="list">
					<li>INSTITUTO FEDERAL DE SÃO PAULO</li>
					@if (Auth::guest())
					<li>
						<button><a class="LinkBranco" href="{{ route('login') }}">LOGIN</a></button>
					</li>
					@else
					<li class="dropdown">
						<button data-toggle="dropdown" role="button">
							{{strtoupper(trans(Auth::user()->name))}}
						</button>
						<ul class="dropdown-menu navbar-nav" role="menu">
							<li><a href="{{ route('user.edit',  Auth::user()->id ) }}" class="dropdown-toggle">Meus Dados</a></li>
							<li>
								<a href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();" class="dropdown-toggle">
								Logout</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
					@endif
				</ul>
			</div>

			<div class="collapse navbar-collapse lista" id="app-navbar-collapse">
				<ul class="nav navbar-nav u">
					@if (Auth::guest())
					<li>
						<a href="{{ route('home') }}">
							Home
						</a>
						@if (Request::is('home'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
					</li>
					<li>
						<a href="{{ route('course.index') }}">
							Nossos Cursos
						</a>
						@if (Request::is('course') OR Request::is('course/*'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Cadastre-se
						</a>
						@if (Request::is('proponent') OR Request::is('proponent/create') OR Request::is('student') OR Request::is('student/create'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
						<ul class="dropdown-menu">
							<li class="dropdown-header">Cadastros</li>
							<li><a href="{{ route('proponent.create') }}">Seja um Proponente</a></li>
							<li><a href="{{ route('student.create') }}">Seja um Aluno</a></li>
						</ul>
					</li>
					@else
					<li>
						<a href="{{ route('home') }}">
							Home
						</a>
						@if (Request::is('home'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
					</li>
					@can('has-permission', 'employee')
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Cadastros
						</a>
						@if (Request::is('proponent/create') OR Request::is('student/create') OR Request::is('register') OR Request::is('propagation/create') OR Request::is('course/create'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
						<ul class="dropdown-menu">
							<li class="dropdown-header">Cadastros</li>
							<li><a href="{{ route('proponent.create') }}">Proponente</a></li>
							<li><a href="{{ route('propagation.create') }}">Divulgação</a></li>
							<li><a href="{{ route('register') }}">Funcionario</a></li>
							<li><a href="{{ route('course.create') }}">Cursos</a></li>
						</ul>
					</li>
					@endcan
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Cursos
						</a>
						@if (Request::is('propagation') OR Request::is('course'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
						<ul class="dropdown-menu">
							<li><a href="{{ route('course.index') }}">Cursos</a></li>
							@can('has-permission', 'employee')
							<li><a href="{{ route('propagation.index') }}">Divulgação</a></li>
							@endcan
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Turmas
							@can('not-student')
							<span class="badge badge-light">{{$requestsCursos}}</span>
							@endcan
						</a>
						@if (Request::is('turas') OR Request::is('turmas/*'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
						<ul class="dropdown-menu">
							<li class="dropdown-header">Solicitações</li>
							@can('not-student')
							<li><a href="{{ route('turma.index') }}">Solicitações</a></li>
							@endcan
							@can('has-permission', 'employee')
							<li><a href="{{ route('turma.approved') }}">Aprovadas</a></li>
							<li><a href="{{ route('turma.canceled') }}">Canceladas</a></li>
							<li><a href="{{ route('turmas.indexAbertas') }}">Abertas</a></li>
							<li><a href="{{ route('turma.index') }}">Concluidas</a></li>
							@endcan
						</ul>
					</li>
					@can('has-permission', 'employee')
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Proponentes <span class="badge badge-light">{{$requestsProponent}}</span>
						</a>
						@if (Request::is('proponent') OR Request::is('proponent/solicitacoes/aprovar'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
						<ul class="dropdown-menu">
							<li><a href="{{ route('proponent.index') }}">Proponente</a></li>
							<li><a href="{{ route('proponent.ap') }}">Solicitações</a></li>
						</ul>
					</li>
					@endcan
					@can('has-permission', 'employee')
					<li>
						<a href="{{ route('employee.index') }}">
							Funcionario
						</a>
						@if (Request::is('employee'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
					</li>
					@endcan
					@can('has-permission', 'employee')
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Editais
						</a>
						@if (Request::is('notice') OR Request::is('notice/*'))
						<div class="linhaMenuOn"></div>
						@else
						<div class="linhaMenu"></div>
						@endif
						<ul class="dropdown-menu">
							<li><a href="{{ route('notice.create') }}">Cadastro</a></li>
							<li><a href="{{ route('notice.index') }}">Em Andamento</a></li>
							<li><a href="{{ route('notice.indexPublicado') }}">Publicados</a></li>
						</ul>
					</li>
					@endcan
					@endif
				</ul>
			</div>
		</div>
	</nav>

	<div class="wrapper">
		<div class="fundo body-content" id="app">
			@if (Session::has('message'))
			<div  class="alert col-md-10 col-md-offset-1 alert-success">{{ Session::get('message') }}</div>
			@endif

			@if (Session::has('messageErro'))
			<div  class="alert col-md-10 col-md-offset-1 alert-danger">{{ Session::get('messageErro') }}</div>
			@endif

			@yield('content')
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h5 class="white-text">IFSP</h5>
					<p class="grey-text text-lighten-4">Coordenadoia de Extensão do Instituto Federal de São Paulo <br />Campus Guarulhos.</p>
				</div>
				<div class="col-md-3">
					<h5 class="white-text">IFSP</h5>
					<ul>
						<li><a class="white-text" href="http://portal.ifspguarulhos.edu.br/index.php/coordenadoria.html">CEX</a></li>
						<li><a class="white-text" href="http://portal.ifspguarulhos.edu.br/index.php/instituicao.html">Instituição</a></li>
						<li><a class="white-text" href="http://portal.ifspguarulhos.edu.br/index.php/apoio-ao-aluno.html">Apoio ao Aluno</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h5 class="white-text">CEX</h5>
					<ul>
						<li><a class="white-text" href="http://portal.ifspguarulhos.edu.br/index.php/estagios.html">Estágios</a></li>
						<li><a class="white-text" href="#http://portal.ifspguarulhos.edu.br/index.php/cursos-fic.html">Cursos</a></li>
						<li><a class="white-text" href="http://portal.ifspguarulhos.edu.br/index.php/eventos.html">Eventos</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="page-footer col-md-12 text-center">
			© 2018 Copyright:
			<a href="http://portal.ifspguarulhos.edu.br/index.php/coordenadoria.html">CEX 2018</a>
		</div>
	</footer>

	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
