@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Editais</p></div>
			<div class="panel-body putmargin">
				@if (isset($notice->id))
				<form class="form-horizontal my-from" method="POST" action="{{ route('notice.update', $notice->id) }}">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					@else
					<form class="form-horizontal my-from" method="POST" action="{{ route('notice.store') }}">
						{{ csrf_field() }}
						@endif
						<div class="col s12">
							<select name="t[]" multiple>
								<option value="" disabled>Selecione as Turmas</option>
								@foreach ($allTurmas as $turma  => $value)
								<option value="{{$turma}}"
								@foreach ($notice->turmas as $t)
								{{ $t->id == $turma || old('t') == $value  ?  'selected' : '' }}
								@endforeach
								>{{$value}}</option>
								@endforeach
							</select>
							<label>Turmas</label>
							@if ($errors->has('t'))
							<span>
								<strong class="red-text">{{ $errors->first('t') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s6">
							<label for="number">Numero do edital</label>
							<input type="text"  value="{{ $notice->number ?  $notice->number : old('number') }} " name="number">
							@if ($errors->has('number'))
							<span>
								<strong class="red-text">{{ $errors->first('number') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s12">
							<label for="postResult">Data da publicação do resultado *</label>
							<input type="text"  value="{{ $notice->postResult ?  $notice->postResult : old('postResult') }} " name="postResult" class="datepicker">
							@if ($errors->has('postResult'))
							<span>
								<strong class="red-text">{{ $errors->first('postResult') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s6">
							<label for="registrationstartdate">Data de inicio da Matricula</label>
							<input type="text"  value="{{ $notice->registrationstartdate ?  $notice->registrationstartdate : old('registrationstartdate') }} " name="registrationstartdate" class="datepicker">
							@if ($errors->has('registrationstartdate'))
							<span>
								<strong class="red-text">{{ $errors->first('registrationstartdate') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s6">
							<label for="registrationclosingdate">Data de termino das Matricula</label>
							<input type="text"  value="{{ $notice->registrationclosingdate ?  $notice->registrationclosingdate : old('registrationclosingdate') }} " name="registrationclosingdate" class="datepicker">
							@if ($errors->has('registrationclosingdate'))
							<span>
								<strong class="red-text">{{ $errors->first('registrationclosingdate') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s6">
							<label for="startdate" >Data de inicio das inscrições</label>
							<input type="text" value="{{ $notice->startdate ?  $notice->startdate : old('startdate') }} " name="startdate" class="datepicker">
							@if ($errors->has('startdate'))
							<span>
								<strong class="red-text">{{ $errors->first('startdate') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s6">
							<label for="closingdate">Data de termino das inscrições</label>
							<input type="text"  value="{{ $notice->closingdate ?  $notice->closingdate : old('closingdate') }} " name="closingdate" class="datepicker">
							@if ($errors->has('closingdate'))
							<span>
								<strong class="red-text">{{ $errors->first('closingdate') }}</strong>
							</span>
							@endif
						</div>
						<div class="col s12">
							<label for="testDay">Data da Prova</label>
							<input type="text"  value="{{ $notice->testDay ?  $notice->testDay : old('testDay') }} " name="testDay" class="datepicker">
							@if ($errors->has('testDay'))
							<span>
								<strong class="red-text">{{ $errors->first('testDay') }}</strong>
							</span>
							@endif
						</div>




						<div class="form-group">
							<button type="submit" class="btn bnt-padrao col-md-3">
								ENVIAR
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection
