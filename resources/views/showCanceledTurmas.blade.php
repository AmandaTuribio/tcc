@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Solicitações de turmas canceladas</p></div>
			<div class="panel-body putmargin">
					<vc-paginate  source="/tcc/public/api/turma/requested/canceled"  Componente="RequestedTurma" auth="avaliada"></vc-paginate>
			</div>
		</div>
	</div>
</div>
@endsection
