@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Divulgação de cursos</p></div>
			<div class="panel-body putmargin">
				<vc-paginate  source="/tcc/public/api/propagation" Componente="Propagation"></vc-paginate>
			</div>
		</div>
	</div>
</div>
@endsection
