@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="tituloPag"><p>Cursos Aprovados</p></div>
			<div class="panel-body putmargin">
				@if (isset($course->id))
					<form class="form-horizontal my-from" method="POST" action="{{ route('course.update', $course->id) }}">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
				@else
					<form class="form-horizontal my-from" method="POST" action="{{ route('course.store') }}">
						{{ csrf_field() }}
				@endif
						<div class="form-group{{ $errors->has('proponent_id') ? ' has-error' : '' }}">
							<label class="my-form-label">Proponente</label>
							<div class="input-field">
								<select class="form-control" name="proponent_id" autofocus>
									<option value="" disabled selected>Selecione o Proponente</option>
									@foreach ($allProponents as $proponent  => $value)
									<option value="{{$proponent}}" {{ $course->proponent_id == $proponent || old('proponent_id') == $proponent  ?  'selected' : '' }}>{{$value}}</option>
									@endforeach
								</select>
							</div>
							@if ($errors->has('proponent_id'))
							<span class="help-block">
								<strong>{{ $errors->first('proponent_id') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="my-form-label">Nome do Curso</label>
							<div class="input-field">
								<input id="name" type="text" name="name" value="{{ $course->name ?  $course->name : old('name') }}" class="form-control" required >
							</div>
							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="my-form-label">Descrição</label>
							<div class="input-field">
								<textarea id="description" class="form-control" name="description" required>{{ $course->description ?  $course->description : old('description') }}</textarea>
							</div>
							@if ($errors->has('description'))
							<span class="help-block">
								<strong>{{ $errors->first('description') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('editalAprove') ? ' has-error' : '' }}">
							<label for="editalAprove" class="my-form-label">Aprovado no edital</label>
							<div class="input-field">
								<input id="editalAprove" class="form-control" type="text" name="editalAprove" value="{{ $course->editalAprove ?  $course->editalAprove : old('editalAprove') }}" required>
								@if ($errors->has('editalAprove'))
								<span>
									<strong class="help-block">{{ $errors->first('editalAprove') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('preRequirements') ? ' has-error' : '' }}">
							<label for="preRequirements" class="my-form-label">Pré-Requisitos para o curso</label>
							<div class="input-field">
								<textarea id="preRequirements" class="form-control" name="preRequirements" required>{{ $course->preRequirements ?  $course->preRequirements : old('preRequirements') }} </textarea>
								@if ($errors->has('preRequirements'))
								<span>
									<strong class="help-block">{{ $errors->first('preRequirements') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('workload') ? ' has-error' : '' }}">
							<label for="workload" class="my-form-label">Carga horaria</label>
							<div class="input-field">
								<input type="number" class="form-control" value="{{ $course->workload ? $course->workload : old('workload')}}" name="workload">
								@if ($errors->has('workload'))
								<span>
									<strong class="help-block">{{ $errors->first('workload') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<vc-form-course data="{{ $course->educationalLevel ? $course->educationalLevel : old('educationalLevel')}}"  initial="{{ $course->initialAge ?  $course->initialAge : old('initialAge') }}" final="{{ $course->finalAge ?  $course->finalAge : old('finalAge') }}"></vc-form-course>

						<div class="form-group{{ $errors->has('finalAge') || $errors->has('initialAge') ? ' has-error' : '' }}">
							@if ($errors->has('finalAge'))
							<span class="help-block">
								<strong>{{ $errors->first('finalAge') }}</strong>
							</span>
							@endif
							@if ($errors->has('initialAge'))
							<span class="help-block">
								<strong>{{ $errors->first('initialAge') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group col-md-12">
							<button type="submit" class="btn bnt-padrao col-md-3">
								ENVIAR
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	@endsection
