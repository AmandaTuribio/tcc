<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{
	use Notifiable;

	protected $fillable = [
		'name', 'email', 'password','active', 'type'
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	public function hasPermission(){
		return $this->type;
	}

	public function employee(){
		return $this->hasOne('App\Employee');
	}

	public function proponent(){
		return $this->hasOne('App\Proponent');
	}

}
