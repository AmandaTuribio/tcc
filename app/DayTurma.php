<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayTurma extends Model{

	protected $fillable = [
	   'turma_id', 'day_id'
   ];
}
