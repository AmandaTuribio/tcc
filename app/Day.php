<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model{

    protected $fillable = [
    	'description'
    ];

	public function turmas(){
    	return $this->belongsToMany('App\Turma', 'days_turmas');
    }
}
