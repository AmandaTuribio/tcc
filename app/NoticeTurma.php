<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticeTurma extends Model{

	protected $fillable = [
		'notice_id', 'turma_id'
	];
}
