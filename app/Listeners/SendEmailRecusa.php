<?php

namespace App\Listeners;

use App\Events\RecusaProponent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\mailController;

class SendEmailRecusa
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RecusaProponent  $event
     * @return void
     */
    public function handle(RecusaProponent $event)
    {
        mailController::sendRecuse($event->proponent);
    }
}
