<?php

namespace App\Listeners;

use App\Events\AprovaSolicitação;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\mailController;

class SendEmailNotice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AprovaSolicitação  $event
     * @return void
     */
    public function handle(AprovaSolicitação $event){
        mailController::sendNotice($event->notice);
    }
}
