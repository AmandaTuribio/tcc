<?php

namespace App\Listeners;

use App\Events\InsertProponent;
use App\Http\Controllers\mailController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail{

    public function __construct(){
        //
    }

    public function handle(InsertProponent $event){
        mailController::send($event->proponent);
    }
}
