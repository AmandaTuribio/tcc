<?php

namespace App\Listeners;

use App\Events\publicar;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\mailController;

class SendEmailpublicar
{
	/**
	* Create the event listener.
	*
	* @return void
	*/
	public function __construct()
	{
		//
	}

	/**
	* Handle the event.
	*
	* @param  publicar  $event
	* @return void
	*/
	public function handle(publicar $event){
		mailController::sendSolicitacaoPublicacao($event->notice);
		mailController::sendDivulgacao($event->notice);
	}
}
