<?php

namespace App\Listeners;

use App\Events\AprovaEdicao;
use Illuminate\Queue\InteractsWithQueue;
use App\Http\Controllers\mailController;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailAprove
{
    public function __construct()
    {
        //
    }

    public function handle(AprovaEdicao $event)
    {
		mailController::sendAprovaEdi($event->turma);
    }
}
