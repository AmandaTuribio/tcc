<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model{

	protected $fillable = [
	   'course_id', 'proponent_id','status', 'date', 'startTime', 'closingTime', 'selectiveProcess',
	   'classroom', 'startdate', 'closingdate', 'numberVacancies', 'refused', 'approvedEdition', 'name'
   ];

   public function days(){
	   return $this->belongsToMany('App\Day', 'days_turmas');
   }

   public function notices(){
   		return $this->belongsToMany('App\Notice', 'notice_turmas');
   }

   public function proponent(){
	   return $this->belongsTo('App\Proponent', 'proponent_id');
   }

   public function course(){
	   return $this->belongsTo('App\Course', 'course_id');
   }
}
