<?php

namespace App;

use PDF;
use Illuminate\Database\Eloquent\Model;

class Notice extends Model{

	protected $fillable = [
		'date', 'number', '_token',

		'startdate', 'closingdate', 'registrationstartdate', 'registrationclosingdate', 'testDay', 'postResult', 'status'
	];

	public function turmas(){
		return $this->belongsToMany('App\Turma', 'notice_turmas');
	}

	public static function getEdital($id){
		$notice 	= Notice::with('turmas', 'turmas.course', 'turmas.days')->findOrFail($id);
		PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
		$pdf = PDF::loadView('edital', compact('notice'));
		return $pdf;
	}
}
