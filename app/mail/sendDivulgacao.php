<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Propagation;

class sendDivulgacao extends Mailable{
	use Queueable, SerializesModels;

	public $notice;

	public function __construct($notice){
		$this->notice = $notice;
	}

	public function build(){
		$mail = $this->view('mailSendDivulgacao', ['notice' => $this->notice])
		->subject("Sistema CEX");

		$Propagation = Propagation::all();

		foreach ($Propagation as $pessoa) {
			$mail = $mail->cc($pessoa->email, $pessoa->name);
		}

		return $mail;
	}
}
