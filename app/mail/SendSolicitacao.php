<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

class SendSolicitacao extends Mailable{
	use Queueable, SerializesModels;

	public $notice;

	public function __construct($notice){
		$this->notice = $notice;
	}

	public function build(){
		$mail = $this->view('mailSendSolicitacao', [
			'notice' => $this->notice,
         'link' => url(route('notice.cadastrarNumero',['id' => $this->notice->id, 'token' => $this->notice->_token ] )) ])
			->to('amandaabreu81422@gmail.com')
			->subject("Sistema CEX");
		return $mail;
	}
}
