<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Proponent;
use App\User;
use Auth;

class SendMail extends Mailable{
	use Queueable, SerializesModels;

	public $proponent;

	public function __construct($proponent){
		$this->proponent = $proponent;
	}

	public function build(){
		$mail = $this->view('mail', ['proponent' => $this->proponent])
		->to($this->proponent->user->email, $this->proponent->user->name)
		->subject("Sistema CEX");

		return $mail;
	}
}
