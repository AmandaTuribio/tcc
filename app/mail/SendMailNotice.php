<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Notice;
use Auth;

class SendMailNotice extends Mailable{
	use Queueable, SerializesModels;

	public $notice;

	public function __construct($notice){
		$this->notice = $notice;
	}

	public function build(){
		// $mail = $this->view('mailNotice', ['notice' => $this->notice])->to($this->notice->turmas->proponent->user->email, $this->notice->turmas->proponent->user->name)->subject("Edital de Curso");
    //
		// return $mail;
	}
}
