<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

class SendAprovaEdi extends Mailable{
	use Queueable, SerializesModels;

	public $turma;

	public function __construct($turma){
		$this->turma = $turma;
	}

	public function build(){
		$mail = $this->view('mailEditAprov', ['turma' => $this->turma, 'link' => url(route('turma.index'))])
		->to($this->turma->proponent->user->email, $this->turma->proponent->user->name)
		->subject("Sistema CEX");

		return $mail;
	}
}
