<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Notice;

class sendSolicitacaoPublicacao extends Mailable{
	use Queueable, SerializesModels;

	public $notice;

	public function __construct($notice){
		$this->notice = $notice;
	}

	public function build(){
		$pdf = Notice::getEdital($this->notice->id);
		$mail = $this->view('mailSendSolicitacaoPublicacao', [
			'notice' => $this->notice,
			'linkImage' => url(route('course.index'))
			])->to('amandaabreu81422@gmail.com')//CTI
			->subject("Sistema CEX")
			->attachData($pdf->output(), 'Edital.pdf', [
				'mime' => 'application/pdf',
			]);


			return $mail;
		}
	}
