<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Proponent;
use App\Turma;
use Auth;

class AppServiceProvider extends ServiceProvider{

	public function boot(){
		Schema::defaultStringLength(191);
		$requestsProponent = $this->getRequestsProponent();
		View::share('requestsProponent', $requestsProponent);
		\View::composer('*', function($view){
			$view->with('requestsCursos', $this->getRequestsCursos());
		});
	}

	public function getRequestsProponent(){
		$count = Proponent::where('status','Solicitado')->count();
		return $count;
	}

	public function getRequestsCursos(){
		if (Auth::user()) {
			if (\Auth::user()->type == 'proponent') {
				$count = Turma::where('status', 'solicitado')
				->where('proponent_id', 'LIKE', \Auth::id())
				->where('approvedEdition', false)
				->count();
			}else {
				$count = Turma::where('status', 'solicitado')
				->where('approvedEdition', true)
				->count();
			}
			return $count;
		}
	}

	public function register(){
		//
	}
}
