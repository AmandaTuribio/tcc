<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
	    'App\Events\InsertProponent' => [
	        'App\Listeners\SendEmail',
	    ],
		'App\Events\RecusaProponent' => [
			'App\Listeners\SendEmailRecusa',
		],
		'App\Events\AprovaSolicitação' => [
			'App\Listeners\SendEmailNotice',
		],
		'App\Events\AprovaEdicao' => [
			'App\Listeners\SendEmailAprove',
		],
		'App\Events\publicar' => [
			'App\Listeners\SendEmailpublicar',
		],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
