<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
	/**
	* The policy mappings for the application.
	*
	* @var array
	*/
	protected $policies = [
		'App\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	* Register any authentication / authorization services.
	*
	* @return void
	*/
	public function boot()
	{
		$this->registerPolicies();

		Gate::define('has-permission', function($user, $type){
			return $user->hasPermission() == $type;
		});

		Gate::define('not-student', function($user){
			return $user->hasPermission() != 'student';
		});

		Gate::define('is-employee-or-himself', function($user, $id){
			return $user->hasPermission() == 'employee' or $user->id == $id;
		});
	}
}
