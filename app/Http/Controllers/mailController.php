<?php

namespace App\Http\Controllers;

use Mail;
use Auth;
use App\mail\SendMail;
use App\mail\SendAprovaEdi;
use Illuminate\Http\Request;
use App\mail\SendMailRecuse;
use App\mail\SendMailNotice;
use App\mail\sendDivulgacao;
use App\mail\SendSolicitacao;
use App\mail\sendrequestProntuario;
use App\mail\sendSolicitacaoPublicacao;

class mailController extends Controller{

	public static function send($model){
		Mail::send(new SendMail($model));
	}

	public static function sendRecuse($model){
		Mail::send(new SendMailRecuse($model));
	}

	public static function sendSolicitacao($model){
		Mail::send(new SendSolicitacao($model));
	}

	public static function sendAprovaEdi($model){
		Mail::send(new SendAprovaEdi($model));
	}

	public static function sendSolicitacaoPublicacao($model){
		Mail::send(new sendSolicitacaoPublicacao($model));
	}

	public static function sendDivulgacao($model){
		Mail::send(new sendDivulgacao($model));
	}






	public static function sendNotice($model){
		Mail::send(new SendMailNotice($model));
	}

}
