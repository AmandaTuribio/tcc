<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\ProponentRequest;
use App\Events\InsertProponent;
use App\Events\RecusaProponent;
use Illuminate\Http\Request;
use App\Proponent;
use App\User;

class ProponentController extends Controller{

	private $proponent;
	private $user;

	public function __construct(Proponent $proponent, User $user){
		$this->proponent 	= $proponent;
		$this->user 		= $user;
		$this->middleware('auth', ['except' => ['create', 'store']]);
	}

	public function index(){
		$this->authorize('has-permission', 'employee');
		return view('showProponents');
	}

	public function showProponentAprovado(){
		$this->authorize('has-permission', 'employee');
		return view('showProponentsApove');
	}

	public function getData($type){
		$this->authorize('has-permission', 'employee');
		if($type == "solicitados"){
			$model = $this->filterAprova();
		}else{
			$model = $this->filter();
		}
		return response()
		->json([
			'model' => $model
		]);
	}

	public function filter(){
		$this->authorize('has-permission', 'employee');
		$request = app()->make('request');
		if($request->has('search_input') && $request->search_input != '') {

			$model = Proponent::whereHas('user', function ($query) use($request) {
				$query->where('name', 'LIKE', '%'.$request->search_input.'%');
			 })
			 	->where('status','Aprovado')
				->with('user')
				->paginate(6);
		}else{
			$model = Proponent::with('user')->where('status','Aprovado')->paginate(6);
		}
		return $model;
	}

	public function filterAprova(){
		$this->authorize('has-permission', 'employee');
		$request = app()->make('request');
		if($request->has('search_input') && $request->search_input != '') {

			$model = Proponent::whereHas('user', function ($query) use($request) {
				$query->where('name', 'LIKE', '%'.$request->search_input.'%');
			 })
			 	->where('status','Solicitado')
				->with('user')
				->paginate(6);
		}else{
			$model = Proponent::with('user')->where('status','Solicitado')->paginate(6);
		}
		return $model;
	}

	public function create(User $user){
		return view('proponent', compact('user'));
	}

	public function store(ProponentRequest $request){
		$dataForm 				= $request->all();
		$dataForm['active'] 	= false;
		$dataForm['type'] 		= 'proponent';
		$dataForm['password'] 	= bcrypt($dataForm['password']);
		$user	 				= $this->user->create($dataForm);
		$data['user_id']		= $user->id;
		$data['type']			= $dataForm['typeProp'];
		$data['status'] 		= 'Solicitado';
		$insert 				= $this->proponent->create($data);
		if ($insert) {
			$proponent 			= Proponent::find($user->id);
			event(new InsertProponent($proponent));
		}
		Session::flash('message', "Proponente cadastrado!");
		return redirect()->route('home');
	}

	public function edit($id){
		$this->authorize('is-employee-or-himself', $id);
		$user	= User::with('proponent')->findOrFail($id);
		return view('proponent', compact('user'));
	}

	public function toApprove($id){
		$this->authorize('has-permission', 'employee');
		$user				= User::findOrFail($id);
		$proponent			= Proponent::findOrFail($id);
		$data['active'] 	= true;
		$dataP['status'] 	= 'Aprovado';
		$update 			= $user->update($data);
		$update 			= $proponent->update($dataP);
		if ($update) {
			$proponent 			= Proponent::find($id);
			event(new InsertProponent($proponent));
		}
		return redirect()->route('proponent.ap');
	}

	public function refuse($id){
		$this->authorize('has-permission', 'employee');
		$proponent 	= Proponent::find($id);
		event(new RecusaProponent($proponent));
		$user	= User::with('proponent')->findOrFail($id);
		$user->delete();
		return redirect()->route('proponent.ap');
	}

	public function update(ProponentRequest $request, $id){
		$this->authorize('is-employee-or-himself', $id);
		$dataForm 			= $request->all();
		$user 				= User::find($id);
		$dataForm['active']	= (isset($dataForm['active']) && $dataForm['active'] == 1 ) ? true : false;
		$update 			= $user->update($dataForm);
		$proponent 			= Proponent::where('user_id', $user->id);
		$data['type']		= $dataForm['typeProp'];
		$update 			= $proponent->update($data);
		return redirect()->route('proponent.index');
	}
}
