<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/employee';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		return Validator::make($data, [
            'name'      => 'required|string|max:255|min:3',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
				'coordinator'=> 'nullable|unique:employees,coordinator',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		 // $this->authorize('has-permission', 'employee');
		$user = User::create([
            'name'    => $data['name'],
            'email'   => $data['email'],
            'password'=> bcrypt($data['password']),
            'active'  => true,
            'type'    => 'employee',
        ]);

        $employee = Employee::create([
          'user_id'     => $user->id,
          'coordinator' => (isset($data['coordinator']) && $data['coordinator'] == 1 ) ? true : false,
        ]);

        return $user;
    }
}
