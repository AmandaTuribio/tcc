<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\TurmaRequest;
use Illuminate\Http\Request;
use App\Events\AprovaEdicao;
use App\Events\AprovaSolicitação;
use App\Course;
use App\Turma;
use App\Day;
use Auth;

class TurmaController extends Controller{
	private $turma, $day;

	public function __construct(Turma $turma, Day $day){
		$this->turma	= $turma;
		$this->day 		= $day;
		setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
		date_default_timezone_set('America/Sao_Paulo');
		$this->middleware('auth');
	}

	//Listar solicitações
	public function index(){
		$this->authorize('not-student');
		return view('showRequestedTurmas');
	}

	public function getData(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter();
		return response()
		->json([
			'model' => $model
		]);
	}

	public function toAprove(){
		$this->authorize('has-permission', 'proponent');
		$model = $this->filter(false, Auth::id());
		return response()
		->json([
			'model' => $model
		]);
	}

	public function approved(){
		$this->authorize('has-permission', 'employee');
		return view('showApprovedTurmas');
	}

	public function indexAbertas(){
		$this->authorize('has-permission', 'employee');
		return view('showTurmasAbertas');
	}

	public function showAbertas(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter(true, '%%', 'aberta');
		return response()
		->json([
			'model' => $model
		]);
	}

	public function showApproved(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter(true, '%%', 'aprovada');
		return response()
		->json([
			'model' => $model
		]);
	}

	public function canceled(){
		$this->authorize('has-permission', 'employee');
		return view('showCanceledTurmas');
	}

	public function showCanceled(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter(true, '%%', 'cancelada');
		return response()
		->json([
			'model' => $model
		]);
	}

	public function filter($toAprove = true, $proponent = '%%', $status = 'solicitado'){
		$request = app()->make('request');
		if($request->has('search_input') && $request->search_input != '') {
			$model = Turma::whereHas('course', function ($query) use($request) {
				$query->where('name', 'LIKE', '%'.$request->search_input.'%');
			})
			->with('days')
			->with('proponent', 'proponent.user')
			->with('course')
			->where('status', $status)
			->where('proponent_id', 'LIKE', $proponent)
			->where('approvedEdition', $toAprove)
			->paginate(6);
		}else{
			$model = Turma::with('days')
			->with('proponent', 'proponent.user')
			->with('course')
			->where('status', $status)
			->where('proponent_id', 'LIKE', $proponent)
			->where('approvedEdition', $toAprove)
			->paginate(6);
		}
		return $model;
	}

	//Utilitarios
	public function makeDaysArray(){
		$allDay = $this->day->get();
		$days 	= Array();
		foreach ($allDay as $day) {
			$days[$day->id] = $day->description;
		}
		return $days;
	}

	//Cadastra uma solicitação
	public function create(Turma $turma, $id){
		$this->authorize('has-permission', 'proponent');
		$course = Course::find($id);
		$days 	= $this->makeDaysArray();
		return view('turma', compact('course','turma', 'days'));
	}

	public function store(TurmaRequest $request){
		$this->authorize('has-permission', 'proponent');
		$dataForm 					= $request->all();
		$dataForm['proponent_id']	= Auth::id();
		$dataForm['status']			= 'solicitado';
		$dataForm['date']				= date('Y/m/d');
		$dataForm['approvedEdition']= true;
		$insert 					= $this->turma->create($dataForm);
		$turma 						= Turma::findOrFail($insert->id);
		foreach ($dataForm['d'] as $day) {
			$turma->days()->attach($day);
		}
		Session::flash('message', "Solicitação cadastrada!");
		return redirect()->route('course.index');
	}

	//Editar solicitação
	public function editData($idTurma, $idCurso){
		$title  = 'Editar Solicitação de Curso';
		$course = Course::find($idCurso);
		$turma 	= Turma::find($idTurma);
		$this->authorize('is-employee-or-himself', $turma->proponent_id);
		$days 	= $this->makeDaysArray();
		return view('turma', compact('turma', 'title', 'course', 'days'));
	}

	public function update(TurmaRequest $request, $id){
		$dataForm 	= $request->all();
		if(Auth::user()->type == 'employee'){
			$dataForm['approvedEdition'] = false;
		}
		$turma 		= Turma::find($id);
		$this->authorize('is-employee-or-himself', $turma->proponent_id);
		$update 	= $turma->update($dataForm);
		if (isset($dataForm['approvedEdition'])) {
			$turma 		= Turma::find($id);
			event(new AprovaEdicao($turma));
		}
		return redirect()->route('turma.index');
	}

	public function show($id){
		return 'show';
	}

	public function edit(Request $request, $id){
		$dataForm 				= $request->all();
		$dataForm['status']	= 'cancelada';
		$dataForm['approvedEdition'] = true;
		$turma 					= Turma::find($id);
		$this->authorize('is-employee-or-himself', $turma->proponent_id);
		$update 					= $turma->update($dataForm);
		if (Auth::user()->type != 'proponent') {
			event(new AprovaEdicao($turma));
		}
		return redirect()->route('turma.index');
	}

	//APROVAR EDIÇAO
	public function aproveEdition($id){
		$turma 		= Turma::find($id);
		$this->authorize('is-employee-or-himself', $turma->proponent_id);
		if (Auth::user()->type == 'proponent') {
			$update 	= $turma->update(array('approvedEdition' => true));
		}else {
			$update 	= $turma->update(array('status' => 'aprovada'));
		}
		return redirect()->route('turma.index');
	}
}
