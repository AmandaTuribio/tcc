<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Http\Request;
use App\Employee;
use App\User;

class EmployeeController extends Controller{

	private $employee;

	public function __construct(Employee $employee){
		$this->employee = $employee;
		$this->middleware('auth');
	}

	public function index(){
		$this->authorize('has-permission', 'employee');
		return view('showEmployees');
	}

	public function getData(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter();
		return response()
		->json([
			'model' => $model
		]);
	}

	public function filter(){
		$this->authorize('has-permission', 'employee');
		$request = app()->make('request');
		if($request->has('search_input') && $request->search_input != '') {
			$model = Employee::whereHas('user', function ($query) use($request) {
				$query->where('name', 'LIKE', '%'.$request->search_input.'%');
			 })
				->with('user')
			    ->paginate(10);
		}else{
			$model = Employee::with('user')->paginate(10);
		}
        return $model;
	}

	public function edit($id){
		$this->authorize('has-permission', 'employee');
		$user	= User::with('employee')->findOrFail($id);
		return view('employee', compact('user'));
	}

	public function update(EmployeeRequest $request, $id){
		$this->authorize('has-permission', 'employee');
		$dataForm					= $request->all();
		$user							= User::find($id);
		$employee					= Employee::where('user_id', $id);
		$dataForm['coordinator']=(isset($dataForm['coordinator']) && $dataForm['coordinator']== 1 ) ? true : false;
		$dataForm['active']		= (isset($dataForm['active']) && $dataForm['active'] == 1 ) ? true : false;
		try {
			$update						= $user->update($dataForm);
			$update						= $employee->update(array('coordinator' => $dataForm['coordinator']));
			Session::flash('message', "Curso cadastrado!");
		} catch (\Exception $e) {Session::flash('message', "Erro ao atualizar dados!");}
		return redirect()->route('employee.index');
	}
}
