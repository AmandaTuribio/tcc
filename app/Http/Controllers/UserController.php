<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller{

	public function __construct(){
		$this->middleware('auth');
	}

	public function edit($id){
		$user = User::find($id);
		if ($user->type=='employee') {
			return redirect()->route('employee.edit', $id);
		}elseif ($user->type=='proponent') {
			return redirect()->route('proponent.edit', $id);
		}else {
		}
		return view('meusDados', compact('user'));
	}

	public function update(UserRequest $request, $id){
		$dataForm 					= $request->all();
		$user 						= User::find($id);
		try {
			if (isset($dataForm['password'])) {
				$dataForm['password'] 	= bcrypt($dataForm['password']);
				$update 				= $user->update($dataForm);
			}else{
				$update 		= $user->update(array('name' => $dataForm['name'], 'email' => $dataForm['email']));
			}
			Session::flash('message', "Dados atualizados");
		} catch (\Exception $e) {Session::flash('messageErro', "Erro ao atualizar dados!");}
		return redirect()->route('home');
	}
}
