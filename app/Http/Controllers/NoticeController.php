<?php

namespace App\Http\Controllers;

use App\Http\Controllers\mailController;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\NoticeRequest;
use Illuminate\Http\Request;
use App\Events\publicar;
use App\Notice;
use App\Turma;
use PDF;

class NoticeController extends Controller{

	private $notice;

	public function __construct(Notice $notice){
		setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
		date_default_timezone_set('America/Sao_Paulo');
		$this->notice 	= $notice;
		$this->middleware('auth', ['except' => ['cadastrarNumero', 'storeNumber', 'pdf']]);
	}

	public function create(Notice $notice){
		$this->authorize('has-permission', 'employee');
		$allTurmas = $this->makeRequestedTurmaArray();
		return view('notice', compact('notice', 'allTurmas'));
	}

	public function makeRequestedTurmaArray(){
		$allTurmas = Turma::with('days')->with('proponent', 'proponent.user')->where('status', 'aprovada')->get();
		$turmas 	= Array();
		foreach ($allTurmas as $turma) {
			$turmas[$turma->id] = $turma->name;
		}
		return $turmas;
	}

	public function store(NoticeRequest $request){
		$this->authorize('has-permission', 'employee');
		$dataForm	= $request->all();
		if (isset($dataForm['number'])) {
			$dataForm['status']	= 'ok';
		}else{
			$dataForm['status']	= 'andamento';
		}
		$insert 	= $this->notice->create($dataForm);
		$notice 	= Notice::findOrFail($insert->id);
		foreach ($dataForm['t'] as $t) {
			$turma 	= Turma::findOrFail($t);
			$update = $turma->update(array('status' => 'em andamento'));
			$notice->turmas()->attach($t);
		}
		return redirect()->route('notice.index');
	}

	public function index(){
		$this->authorize('has-permission', 'employee');
		return view('showNotice');
	}

	public function indexPublicado(){
		$this->authorize('has-permission', 'employee');
		return view('showNoticePublicado');
	}

	public function getData(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter();
		return response()
		->json([
			'model' => $model
		]);
	}

	public function filter(){
		$request = app()->make('request');
		$type[] = $request->type;
		if ($request->type == 'andamento') {
			$type[] = 'ok';
			$type[] = 'aguardandoNumero';
		}
		if($request->has('search_input') && $request->search_input != '') {
			$model = Notice::whereHas('turmas.course', function ($query) use($request) {
				$query->where('name', 'LIKE', '%'.$request->search_input.'%');
			})
			->whereIn('status', $type)
			->with('turmas', 'turmas.course')
			->paginate(6);
		}else{
			$model = Notice::whereIn('status', $type)->with('turmas', 'turmas.course')->paginate(6);
		}
		return $model;
	}

	//EDITAR EDITAL
	public function edit($id){
		$this->authorize('has-permission', 'employee');
		$notice	= Notice::with('turmas', 'turmas.course')->findOrFail($id);
		$allTurmas = $this->makeRequestedTurmaArray();
		foreach ($notice->turmas as $turma) {
			$allTurmas[$turma->id] = $turma->name;
		}
		return view('notice', compact('notice', 'allTurmas'));
	}

	public function update(NoticeRequest $request, $id){
		$this->authorize('has-permission', 'employee');
		$dataForm	= $request->all();
		$notice 	= Notice::with('turmas', 'turmas.course')->findOrFail($id);
		if (isset($dataForm['number'])) {
			$dataForm['status']	= 'ok';
		}elseif(isset($notice->number)){
			$dataForm['status']	= 'andamento';
		}
		$update 	= $notice->update($dataForm);
		foreach ($notice->turmas as $turma) {
			$turma 	= Turma::findOrFail($turma->id);
			$update = $turma->update(array('status' => 'aprovada'));
			$notice->turmas()->detach($turma->id);
		}
		foreach ($dataForm['t'] as $t) {
			$turma 	= Turma::findOrFail($t);
			$update = $turma->update(array('status' => 'em andamento'));
			$notice->turmas()->attach($t);
		}
		return redirect()->route('notice.index');
	}

	//Solicitar numero de prontuario
	public function solicitarNumero($id){
		$this->authorize('has-permission', 'employee');
		$_token	= csrf_token();
		$notice 	= Notice::findOrFail($id);
		$update 	= $notice->update(array('status' => 'aguardandoNumero', '_token' => $_token));
		mailController::sendSolicitacao($notice);
		return redirect()->route('notice.index');
	}

	public function cadastrarNumero($id, $token){
		$notice 	= Notice::findOrFail($id);
		if ($notice->_token != $token) {
			abort(404);
		}
		return view('requestNumberForm', compact('notice'));
	}

	public function storeNumber(Request $request, $id){
		$notice 	= Notice::findOrFail($id);
		$dataForm	= $request->all();
		try {
			$update 	= $notice->update(array('status' => 'ok', '_token' => 0, 'number'=> $dataForm['number']));
			Session::flash('message', "Numero cadastrado com sucesso!");
		} catch (\Exception $e) {
			Session::flash('message', "Erro ao cadastrar numero! Tente novamente ou encaminhe o numero por email. Obrigado!");
		}
		return view('requestOk');
	}

	//BAIXAR PDF DO EDITAL
	public function pdf($id){
		$notice 	= Notice::with('turmas', 'turmas.course', 'turmas.days')->findOrFail($id);
		PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
		$pdf = PDF::loadView('edital', compact('notice'));
		return $pdf->download('edital.pdf');
	}

	//VISUALIZAR EDITAL
	public function edital($id){
		$notice 	= Notice::with('turmas', 'turmas.course', 'turmas.days')->findOrFail($id);
		return view('edital', compact('notice'));
	}

	//PUBLICAR EDITAL
	public function publicar($id){
		$notice 	= Notice::find($id);
		$update 	= $notice->update(array('status' => 'publicado', 'date' =>  date('Y/m/d')));
		foreach ($notice->turmas as $turmas) {
			$update = $turmas->update(array('status' => 'aberta'));
		}
		mailController::sendSolicitacaoPublicacao($notice);
		mailController::sendDivulgacao($notice);
		return redirect()->route('notice.index');
	}


	//DELETAR EDITA
	public function delete($id){
		$notice 	= Notice::with('turmas')->findOrFail($id);
		foreach ($notice->turmas as $turma) {
			$turma 	= Turma::findOrFail($turma->id);
			$update = $turma->update(array('status' => 'aprovada'));
			$notice->turmas()->detach($turma->id);
		}
		$notice->delete();
		return redirect()->route('notice.index');
	}
}
