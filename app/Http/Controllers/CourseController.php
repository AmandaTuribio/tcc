<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\CourseRequest;
use Illuminate\Http\Request;
use App\Proponent;
use App\Course;
use Auth;

class CourseController extends Controller{

	private $course;

	public function __construct(Course $course){
		$this->course = $course;
	}

	public function index(){
		if (Auth::user()) {
			$auth = Auth::user()->type;
		}else {
			$auth = 'visitante';
		}
		return view('showCourses', compact('auth'));
	}

	public function getData(){
		$model = $this->filter();
		return response()
		->json([
			'model' => $model
		]);
	}

	public function filter(){
		$request = app()->make('request');
		if($request->has('search_input') && $request->search_input != '') {
			$model = Course::where('name', 'LIKE', '%'.$request->search_input.'%')
			->with('proponent', 'proponent.user' )
			->paginate(6);
		}else{
			$model = Course::with('proponent', 'proponent.user')->paginate(6);
		}
		return $model;
	}

	public function makeProponentArray(){
		$allProponents	= Proponent::with('user')->get();
		$proponents 	= Array();
		foreach ($allProponents as $proponent) {
			$proponents[$proponent->user_id] = $proponent->user->name;
		}
		return $proponents;
	}

	public function create(Course $course){
		$this->authorize('has-permission', 'employee');
		$allProponents = $this->makeProponentArray();
		return view('course', compact('course', 'allProponents'));
	}

	public function store(CourseRequest $request){
		$this->authorize('has-permission', 'employee');
		$dataForm 	= $request->all();
		try {
			$insert 	= $this->course->create($dataForm);
			Session::flash('message', "Curso cadastrado!");
		} catch (\Exception $e) {	Session::flash('message', "Erro ao cadastrar curso!");}
		return redirect()->route('course.index');
	}

	public function edit($id){
		$this->authorize('has-permission', 'employee');
		$title 			= 'Editar Curso de Extensão';
		$course 			= Course::find($id);
		$allProponents = $this->makeProponentArray();
		return view('course', compact('course', 'allProponents', 'title'));
	}

	public function update(CourseRequest $request, $id){
		$this->authorize('has-permission', 'employee');
		$dataForm = $request->all();
		if($dataForm['educationalLevel'] 	!= 'age'){
			$dataForm['initialAge'] 		= NULL;
			$dataForm['finalAge'] 			= NULL;
		}
		$course = Course::find($id);
		try {
			$update = $course->update($dataForm);
		} catch (\Exception $e) {	Session::flash('message', "Erro ao atualizar curso!");}
		return redirect()->route('course.index');
	}
}
