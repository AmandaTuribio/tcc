<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Student;
use App\User;

class StudentController extends Controller
{
    private $student;
    private $user;

    public function __construct(Student $student, User $user){
        $this->student 	= $student;
        $this->user 	= $user;
        $this->middleware('auth', ['except' => ['create', 'store']]);
        //$this->middleware('CheckEmployee', ['except' => ['create', 'store']]);
    }

    public function index()
    {
        return view('showStudents');
    }

    public function create(Student $student, User $user)
    {
        $ufs = array('AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR',
	    'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO');
        return view('student', compact('user', 'student', 'ufs'));
    }

    public function store(Request $request)
    {
        $dataForm 				= $request->all();
		$dataForm['active'] 	= true;
    //dd($dataForm);
		$dataForm['type'] 		= 'student';
		$dataForm['password'] 	= bcrypt($dataForm['password']);
		$user	 				= $this->user->create($dataForm);
		$dataForm['user_id']		= $user->id;
		$insert 				= $this->student->create($dataForm);

		Session::flash('message', "Aluno cadastrado!");
		return redirect()->route('home');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataForm 			= $request->all();
		$user 				= User::find($id);
        dd($dataForm);
		$update 			= $user->update($dataForm);
		$student 			= Student::where('user_id', $user->id);

		$update 			= $student->update($dataForm);
		return redirect()->route('proponent.index');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
