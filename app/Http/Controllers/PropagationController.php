<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropagationRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Propagation;

class PropagationController extends Controller{

	private $propagation;

	public function __construct(Propagation $propagation){
		$this->propagation 	= $propagation;
		$this->middleware('auth');
	}

	public function create(Propagation $propagation){
		$this->authorize('has-permission', 'employee');
		return view('propagation', compact('propagation'));
	}

	public function store(PropagationRequest $request){
		$this->authorize('has-permission', 'employee');
		$dataForm	= $request->all();
		try {
			$insert 	= $this->propagation->create($dataForm);
			Session::flash('message', "Sucesso ao cadastrar dados!");
		} catch (\Exception $e) {Session::flash('message', "Erro!");}
		return redirect()->route('propagation.index');
	}

	public function index(){
		$this->authorize('has-permission', 'employee');
		return view('showPropagation');
	}

	public function getData(){
		$this->authorize('has-permission', 'employee');
		$model = $this->filter();
		return response()
		->json([
			'model' => $model
		]);
	}

	public function filter(){
		$this->authorize('has-permission', 'employee');
		$request = app()->make('request');
		if($request->has('search_input') && $request->search_input != '') {
			$model = Propagation::where('name', 'LIKE', '%'.$request->search_input.'%')
				->paginate(6);
		}else{
			$model = Propagation::paginate(6);
		}
		return $model;
	}

	public function edit($id){
		$this->authorize('has-permission', 'employee');
		$propagation = Propagation::findOrFail($id);
		return view('propagation', compact('propagation'));
	}

	public function update(PropagationRequest $request, $id){
		$this->authorize('has-permission', 'employee');
		$dataForm		= $request->all();
		$propagation 	= Propagation::findOrFail($id);
		try {
			$update 			= $propagation->update($dataForm);
			Session::flash('message', "Sucesso ao atualizar dados!");
		} catch (\Exception $e) {Session::flash('message', "Erro!");}
		return redirect()->route('propagation.index');
	}

	public function delete($id){
		$this->authorize('has-permission', 'employee');
		$propagation = Propagation::findOrFail($id);
		try {
			$propagation->delete();
			Session::flash('message', "Sucesso ao atualizar dados!");
		} catch (\Exception $e) {Session::flash('message', "Erro!");}
		return redirect()->route('propagation.index');
	}
}
