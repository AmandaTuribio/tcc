<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Redirect;
use Closure;
use Session;

class CheckProponent{

    public function handle($request, Closure $next){

        if ($request->user()->type == 'proponent') {
            return $next($request);
     	}
        Session::flash('message', 'Você não tem permissao para realizar esta tarefa!');
        return Redirect::back();

    }
}
