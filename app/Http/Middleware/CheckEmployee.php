<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Redirect;
use Closure;
use Session;

class CheckEmployee{

	public function handle($request, Closure $next){

		if ($request->user()->type == 'employee') {
			return $next($request);
		}
		Session::flash('message', 'Você não tem permissao para realizar esta tarefa!');
		return Redirect::back();
	}
}
