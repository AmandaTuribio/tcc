<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest{

    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
			'name' 			=> 'required|string|max:255',
			'email' 			=> 'required|string|email|max:255|unique:users,email,'. $this->id,
         'coordinator' 	=> 'nullable|unique:employees,coordinator,'. $this->id.',user_id',
        ];
    }
}
