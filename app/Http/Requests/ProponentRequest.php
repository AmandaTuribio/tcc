<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProponentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|unique:users,email,'. $this->id,
		  'password' => 'string|min:6|confirmed',
        ];
    }
    //
    // public function messages(){
    //
    //   return [
    //       'name.required' => 'Preencha o Nome!',
    //       'name.max' => 'O nome deve ter no maximo 255 caracteres',
    //       'name.string' => 'Preencha o nome corretamente',
    //
    //       'email.required' => 'Preencha o email!',
    //       'email.max' => 'O email deve ter no maximo 255 caracteres',
    //       'email.string' => 'Preencha o email corretamente',
    //       'email.unique' => 'Email ja cadastrado',
    //       'email.email' => 'Preencha o email corretamente',
    //
    //       'password.min' => 'O nome deve ter no minimo 6 caracteres',
    //       'password.string' => 'Preencha a senha corretamente',
    //       'password.confirmed' => 'As senhas devem ser iguais',
    //
    //   ];
    // }
}
