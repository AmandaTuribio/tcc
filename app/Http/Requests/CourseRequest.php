<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
	/**
	* Determine if the user is authorized to make this request.
	*
	* @return bool
	*/
	public function authorize(){
		return true;
	}


	/**
	* Get the validation rules that apply to the request.
	*
	* @return array
	*/
	public function rules()
	{
		return [
			'name'        	=> 'required|string|max:255',
			'proponent_id'  => 'required|exists:proponents,user_id',
			'description' 	=> 'required|string|max:65535',
			'initialAge'  	=> 'nullable|numeric',
			'finalAge'    	=> 'nullable|numeric|min:' . $this->initialAge,
			'editalAprove' 	=>  'required|string',
			'preRequirements'	=> 'required|string|max:255',
			'workload'			=> 'required|numeric|min:1',
		];
	}

	// public function messages(){
	//
	//   return [
	//       'name.required' => 'Preencha o Nome!',
	//       'name.max' => 'O nome deve ter no maximo 255 caracteres',
	//       'name.string' => 'Preencha o nome corretamente',
	//
	//       'description.required' => 'Preencha a descrição!',
	//       'description.max' => 'A descrição deve ter no maximo 65535 caracteres',
	//       'description.string' => 'Preencha a descrição corretamente',
	//
	//       'initialAge.numeric' => 'A idade inicial deve ser um numero',
	//
	//       'finalAge.numeric' => 'A idade final deve ser um numero',
	//
	//       'editalAprove.numeric' => 'O numero do edital deve ser um numero',
	//   ];
	// }
}
