<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TurmaRequest extends FormRequest{
	/**
	* Determine if the user is authorized to make this request.
	*
	* @return bool
	*/
	public function authorize(){
		return true;
	}

	/**
	* Get the validation rules that apply to the request.
	*
	* @return array
	*/
	public function rules()	{
		return [
			'startTime' 		=> 'date_format:H:i:s',
			'closingTime'		=> 'date_format:H:i:s',
			'classroom'			=> 'required|string|max:255',
			'startdate' 		=> 'date',
			'closingdate'		=> 'date',
			'numberVacancies'	=> 'required|numeric|min:1',
			'selectiveProcess'	=> 'required',
			'd'					=> 'required'
		];
	}
}
