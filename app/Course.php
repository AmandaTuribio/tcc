<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model{

	protected $fillable = [
		'name', 'description','workload', 'preRequirements', 'initialAge','finalAge', 'editalAprove', 'proponent_id','educationalLevel'
	];

	public function turmas(){
 	  return $this->hasMany('App\Turma');
    }

	public function proponent(){
 	   return $this->belongsTo('App\Proponent', 'proponent_id');
    }

}
