<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model{

    protected $primaryKey = 'user_id';

    protected $fillable = [
		'user_id', 'street', 'rg', 'cpf', 'number', 'Apto', 'neighborhood', 'city',
        'cep', 'ddd', 'phone', 'celphone', 'birthDate', 'birthCity', 'uf', 'mother',
        'father', 'gender'
	];

	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}
}
