<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Proponent;

class InsertProponent{

    use Dispatchable, InteractsWithSockets, SerializesModels;
	public $proponent;

    public function __construct(Proponent $proponent){
    	$this->proponent = $proponent;
    }

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }
}
