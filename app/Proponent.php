<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proponent extends Model{

	protected $primaryKey = 'user_id';

	protected $fillable = [
		'type', 'user_id', 'status'
	];

	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function turmas(){
		return $this->hasMany('App\Turma');
	}

	public function courses(){
		return $this->hasMany('App\Course');
	}

}
