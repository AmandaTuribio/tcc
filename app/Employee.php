<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model{

  protected $fillable = [
    'coordinator', 'user_id'
  ];

  public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
