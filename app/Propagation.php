<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propagation extends Model{

	protected $fillable = [
		'email', 'name'
	];
}
