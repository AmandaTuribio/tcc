<?php

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('user', 'UserController');

Route::resource('employee', 'EmployeeController');
Route::get('api/employee', 'EmployeeController@getData');

Route::resource('propagation', 'PropagationController');
Route::get('api/propagation', 'PropagationController@getData');
Route::get('/propagation/{id}/delete', 'PropagationController@delete')->name('propagation.delete');

Route::resource('course', 'CourseController');
Route::get('api/course', 'CourseController@getData');

Route::resource('proponent', 'ProponentController');
Route::get('api/proponent/{type}', 'ProponentController@getData');
Route::get('/proponent/solicitacoes/aprovar', 'ProponentController@showProponentAprovado')->name('proponent.ap');
Route::get('/proponent/{id}/aprovar', 'ProponentController@toApprove')->name('proponent.aprovar');
Route::get('/proponent/{id}/recusar', 'ProponentController@refuse')->name('proponent.recusar');

Route::resource('turma', 'TurmaController', ['except' => ['create']]);
Route::get('/turma/create/{id}', 'TurmaController@create')->name('turma.create');
Route::get('/turma/{idTurma}/{idCurso}/editData', 'TurmaController@editData')->name('turma.editData');
Route::get('api/requested/turmas', 'TurmaController@getData');
Route::get('api/requested/turmas/toAprove', 'TurmaController@toAprove');
Route::get('/turma/{id}/aproveEdition', 'TurmaController@aproveEdition');
Route::get('turma/requested/approved', 'TurmaController@approved')->name('turma.approved');
Route::get('api/turma/requested/approved', 'TurmaController@showApproved')->name('turma.showApproved');
Route::get('turma/requested/canceled', 'TurmaController@canceled')->name('turma.canceled');
Route::get('api/turma/requested/canceled', 'TurmaController@showCanceled')->name('turma.showCanceled');
Route::get('turmas/index/Abertas', 'TurmaController@indexAbertas')->name('turmas.indexAbertas');
Route::get('api/turma/index/abertas', 'TurmaController@showAbertas')->name('turma.showAbertas');

Route::resource('notice', 'NoticeController');
Route::get('/notice/{id}/solicitarNumero', 'NoticeController@solicitarNumero')->name('notice.solicitarNumero');
Route::get('/notice/{id}/delete', 'NoticeController@delete')->name('notice.delete');
Route::get('api/notice', 'NoticeController@getData');
Route::get('/notice/{id}/{token}/cadastrarNumero','NoticeController@cadastrarNumero')->name('notice.cadastrarNumero');
Route::post('/notice/storeNumber/{id}', 'NoticeController@storeNumber')->name('notice.storeNumber');
Route::get('/notice/{id}/download', 'NoticeController@pdf')->name('notice.download');
Route::get('/notice/{id}/edital', 'NoticeController@edital')->name('notice.edital');
Route::get('notice/{id}/publicar', 'NoticeController@publicar')->name('notice.publicar');
Route::get('notice/index/Publicados', 'NoticeController@indexPublicado')->name('notice.indexPublicado');









Route::resource('student', 'StudentController');
