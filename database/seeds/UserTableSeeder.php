<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
            'name' => 'Amanda Turibio',
			'email' => 'amandaabreu81422@gmail.com',
			'coordinator' => false,
            'active' => true,
            'password' => bcrypt('secret'),
        ]);
    }
}
