<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('notices', function (Blueprint $table) {
            $table->increments('id');
			$table->date('date')->nullable();
			$table->integer('number')->nullable();
			$table->string('_token')->nullable();
			$table->date('startdate')->nullable();
			$table->date('closingdate')->nullable();
			$table->date('registrationstartdate')->nullable();
			$table->date('registrationclosingdate')->nullable();
			$table->date('testDay')->nullable();
			$table->date('postResult');
			$table->enum('status', ['andamento', 'publicado', 'aguardandoNumero', 'cancelada', 'ok']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
