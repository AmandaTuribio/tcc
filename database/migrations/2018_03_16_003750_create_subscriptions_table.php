<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('subscriptions', function (Blueprint $table) {
			$table->increments('id');
			$table->date('date');
			$table->string('studentHandbook');
			$table->integer('student_id')->unsigned()->default(NULL);
			$table->foreign('student_id')->references('user_id')->on('students')->onDelete('cascade');
			$table->string('specialNeed')->nullable();
			$table->enum('typeVacancy', ['amplaConcorrencia', 'candidatoNegro', 'candidatoComDeficiencia']);
			$table->timestamps();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::dropIfExists('subscriptions');
	}
}
