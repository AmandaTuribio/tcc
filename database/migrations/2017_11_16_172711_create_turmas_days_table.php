<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurmasDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days_turmas', function (Blueprint $table) {
			$table->integer('turma_id')->unsigned();
			$table->foreign('turma_id')->references('id')->on('turmas')->onDelete('cascade');
			$table->integer('day_id')->unsigned();
			$table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');
			$table->primary(['day_id', 'turma_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_c_days');
    }
}
