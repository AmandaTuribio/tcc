<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurmasTable extends Migration{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('turmas', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('course_id')->unsigned();
			$table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
			$table->integer('proponent_id')->unsigned();
			$table->foreign('proponent_id')->references('user_id')->on('proponents')->onDelete('cascade');
			$table->enum('status', ['solicitado', 'aprovada', 'inscricoes', 'em andamento', 'aberta', 'concluido', 'cancelada']);
			$table->date('date');
			$table->time('startTime');
			$table->time('closingTime');
			$table->enum('selectiveProcess', ['sorteio', 'prova', 'ordemInscricao']);
			$table->text('refused')->nullable();
			$table->string('classroom');
			$table->date('startdate');
			$table->date('closingdate');
			$table->boolean('approvedEdition');
			$table->integer('numberVacancies');
			$table->timestamps();
		});

	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::dropIfExists('classesC');
	}
}
