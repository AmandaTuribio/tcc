<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('students', function (Blueprint $table) {
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('rg');
			$table->string('cpf');
			$table->string('street');
			$table->string('number');
			$table->string('Apto')->nullable();
			$table->string('neighborhood');
			$table->string('city');
			$table->string('cep');
			$table->integer('ddd');
			$table->bigInteger('phone');
			$table->bigInteger('celphone');
			$table->date('birthDate');
			$table->string('birthCity');
			$table->string('uf');
			$table->string('mother');
			$table->string('father');
			$table->char('gender', 1);
			$table->timestamps();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::dropIfExists('students');
	}
}
