<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('courses', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('proponent_id')->unsigned();
			$table->foreign('proponent_id')->references('user_id')->on('proponents')->onDelete('cascade');
			$table->integer('workload');
			$table->enum('educationalLevel', ['livre', 'Fundamental I', 'Fundamental II', 'Medio', 'Superior', 'Tecnico', 'age']);
			$table->text('description');
			$table->string('preRequirements')->nullable();
			$table->text('editalAprove');
			$table->integer('initialAge')->nullable();
			$table->integer('finalAge')->nullable();
			$table->timestamps();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::dropIfExists('courses');
	}
}
