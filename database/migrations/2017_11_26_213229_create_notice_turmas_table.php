<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeTurmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notice_turmas', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('notice_id')->unsigned();
			$table->foreign('notice_id')->references('id')->on('notices')->onDelete('cascade');
			$table->integer('turma_id')->unsigned();
			$table->foreign('turma_id')->references('id')->on('turmas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notice_turmas');
    }
}
