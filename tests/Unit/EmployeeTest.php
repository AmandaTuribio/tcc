<?php

namespace Tests\Unit;

use App\Employee;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class EmployeeTest extends TestCase{

use WithoutMiddleware;
private $employee;

    public function testIndex(){
      $this->employee = new Employee();
      $response = $this->get('/employee');
      $response->assertStatus(200);
      $response->assertViewHas('showEmployees', $this->employee);
    }

}
